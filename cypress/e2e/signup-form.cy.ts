const mockSignupData = {
  firstName: 'Vlad',
  lastName: 'Shkurenko',
  email: 'test@gmail.com',
  password: 'qweasdZXC'
}

const passwordErrorMessage = "Password must not contain user\'s first or last name."

describe('Sign-up form', () => {
  beforeEach(() => {
    cy.intercept(
      {
        method: 'POST',
        url: 'https://demo-api.now.sh/users*',
      },
      {
        body: {},
        headers: {
          'Access-Control-Allow-Origin': '*',
        },
      },
    ).as('signupSearchRequest');

    cy.visit('/');
  });

  it('show required error messages', () => {
    cy.findByTestId('firstName').first().clear();
    cy.findByTestId('lastName').first().clear();
    cy.findByTestId('email').first().clear();
    cy.findByTestId('password').first().clear();

    cy.findByTestId('submit').first().click();

    cy.findAllByTestId('control-error').should('have.length', 4).each((error) => {
      expect(error.text()).contains('must be given');
    });
  });

  it('show email error message', () => {
    cy.findByTestId('email').first().clear().type('wrong-email').blur();

    cy.findByTestId('control-error').should('contain.text', 'Please provide a valid email address.');
  });

  it('show password first/last name error messages when password contain first name', () => {
    cy.findByTestId('firstName').first().clear().type(mockSignupData.firstName);
    cy.findByTestId('password').first().clear().type(mockSignupData.firstName).blur();

    cy.findByTestId('control-error').should('contain.text', passwordErrorMessage);
  });

  it('show password first/last name error messages when password contain last name', () => {
    cy.findByTestId('lastName').first().clear().type(mockSignupData.lastName);
    cy.findByTestId('password').first().clear().type(mockSignupData.lastName).blur();

    cy.findByTestId('control-error').should('contain.text', passwordErrorMessage);
  });

  it('submits the data successfully', () => {
    cy.findByTestId('firstName').first().clear().type(mockSignupData.firstName);
    cy.findByTestId('lastName').first().clear().type(mockSignupData.lastName);
    cy.findByTestId('email').first().clear().type(mockSignupData.email);
    cy.findByTestId('password').first().clear().type(mockSignupData.password);

    cy.findByTestId('submit').first().click();

    cy.wait('@signupSearchRequest');

    cy.findByText(/Account has been created/i).should('exist');
  });
});
