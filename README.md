# FedexApp

Due to size of the test assignment there is a pretty common project structure:

```
app
  - components
  - services
  - util
```

## Components

Components folder contain `SignupFormComponent` and `ControlErrorsComponent`.

## Services

`SignupService` creates user and returns user data with uniq Id.

## Utils

Form helpers: `findFormControl` and `validateAllFormControls`.

## Theme

Basic styles for common components `form` and `buttons`, main css variables in `main.scss` and browser reset styles in `reset.scss`.

## Development server

Run `npm run start` for a dev server. Navigate to `http://localhost:4200/`. The application will automatically reload if you change any of the source files.

## Build

Run `npm run build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Running unit tests and generate coverage report

Run `npm run test:coverage` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running Cypress end-to-end tests

Run `npm run cypress:run` to execute the end-to-end tests via a platform of your choice. To use this command, you need to first add a package that implements end-to-end testing capabilities.
