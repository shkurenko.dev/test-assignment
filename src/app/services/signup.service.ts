import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

export interface SignupData {
  firstName: string;
  lastName: string;
  email: string;
}

export interface SignupDataAPIResponse {
  firstName: string;
  lastName: string;
  email: string;
  _id: string;
}

@Injectable({
  providedIn: 'root',
})
export class SignupService {
  constructor(private http: HttpClient) { }

  public signup(data: SignupData): Observable<SignupDataAPIResponse> {
    return this.http.post<SignupDataAPIResponse>(`${environment.signupServiceUrl}/users`, data);
  }
}
