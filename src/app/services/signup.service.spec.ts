import {
  HttpClientTestingModule,
  HttpTestingController,
} from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';

import { SignupDataAPIResponse, SignupService } from './signup.service';

const mockSignupData = {
  firstName: 'Vlad',
  lastName: 'Shkurenko',
  email: 'test@gmail.com',
}

const mockSignupDataResponse = {
  ...mockSignupData,
  _id: '123'
}

describe('SignupService', () => {
  let service: SignupService;
  let controller: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    service = TestBed.inject(SignupService);
    controller = TestBed.inject(HttpTestingController);
  });

  afterEach(() => {
    controller.verify();
  });
  it('signs up', () => {
    let result: SignupDataAPIResponse | undefined;
    service.signup(mockSignupData).subscribe((otherResult) => {
      result = otherResult;
    });

    const request = controller.expectOne({
      method: 'POST',
      url: 'https://demo-api.now.sh/users',
    });
    expect(request.request.body).toEqual(mockSignupData);
    request.flush(mockSignupDataResponse);

    expect(result).toEqual(mockSignupDataResponse);
  });

});
