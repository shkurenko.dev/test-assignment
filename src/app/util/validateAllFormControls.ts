import { FormControl, FormGroup } from "@angular/forms";

export const validateAllFormControls = (formGroup: FormGroup): void => {
  Object.keys(formGroup.controls).forEach(field => {
    const control = formGroup.get(field);

    if (control instanceof FormControl) {
      control.markAsTouched();
    } else if (control instanceof FormGroup) {
      validateAllFormControls(control);
    }
  });
}
