import { HttpErrorResponse } from '@angular/common/http';
import { Component } from '@angular/core';
import { NonNullableFormBuilder, Validators, ValidatorFn, FormGroup, AbstractControl } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { SignupService } from 'src/app/services/signup.service';
import { validateAllFormControls } from 'src/app/util/validateAllFormControls';

const { email, minLength, pattern, required } = Validators;

@Component({
  selector: 'app-signup-form',
  templateUrl: './signup-form.component.html',
  styleUrls: ['./signup-form.component.scss']
})
export class SignupFormComponent {
  public form = this.formBuilder.group(
    {
      firstName: ['', required],
      lastName: ['', [required]],
      email: ['', [required, email]],
      password: ['', [required, minLength(8), pattern('^(?:(?=.*[a-z])(?=.*[A-Z]).*)$')]]
    },
    {
      updateOn: 'blur',
      validators: (ac: AbstractControl) => this.passwordNameValidation(ac as FormGroup)
    }
  );

  constructor(
    private formBuilder: NonNullableFormBuilder,
    private signupService: SignupService,
    private toastr: ToastrService,
  ) { }

  private passwordNameValidation(fg: FormGroup): ReturnType<ValidatorFn> {
    const { firstName, lastName, password } = fg.getRawValue();

    if (
      firstName && password.toLowerCase().includes(firstName.toLowerCase()) ||
      lastName && password.toLowerCase().includes(lastName.toLowerCase())
    ) {
      fg.get('password')?.setErrors({ includesName: true });

      return { includesName: true };
    }

    return null;
  };

  public onSubmit(): void {
    if (this.form.valid) {
      const { password, ...formValue } = this.form.getRawValue();

      this.signupService.signup(formValue).subscribe(
        () => {
          this.toastr.success('Account has been created.', 'Success');
          this.form.reset();
        },
        (err: HttpErrorResponse) => { this.toastr.error(err.error || 'Something went wrong', 'Error') }
      );
    } else {
      validateAllFormControls(this.form);
    }
  }
}
