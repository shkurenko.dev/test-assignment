import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { of } from 'rxjs';
import { ToastrService } from 'ngx-toastr';
import { SignupService } from 'src/app/services/signup.service';
import { ControlErrorsComponent } from '../control-errors/control-errors.component';
import { SignupFormComponent } from './signup-form.component';


const mockSignupData = {
  firstName: 'Vlad',
  lastName: 'Shkurenko',
  email: 'test@gmail.com',
}

const mockSignupDataResponse = {
  ...mockSignupData,
  _id: '123'
}

describe('SignupFormComponent', () => {
  let component: SignupFormComponent;
  let fixture: ComponentFixture<SignupFormComponent>;

  let signupService: jasmine.SpyObj<SignupService>;
  let toastrService: jasmine.SpyObj<ToastrService>;

  signupService = jasmine.createSpyObj<SignupService>('SignupService', {
    signup: of(mockSignupDataResponse)
  });
  toastrService = jasmine.createSpyObj<ToastrService>('ToasterService', ['error', 'success']);

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ReactiveFormsModule],
      declarations: [SignupFormComponent, ControlErrorsComponent],
      providers: [
        {
          provide: SignupService, useValue: signupService
        },
        {
          provide: ToastrService, useValue: toastrService
        }
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SignupFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('submits the form successfully', () => {
    const spyReset = spyOn(component.form, 'reset');

    component.form.setValue({ ...mockSignupData, password: 'qweasdZXC' });
    component.onSubmit();

    expect(signupService.signup).toHaveBeenCalledWith(mockSignupData);
    expect(toastrService.success).toHaveBeenCalled();
    expect(spyReset).toHaveBeenCalled();
  })
});
