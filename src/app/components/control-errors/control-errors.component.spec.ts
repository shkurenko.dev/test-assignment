import {
  Component,
  Type,
} from '@angular/core';
import {
  ComponentFixture,
  TestBed,
} from '@angular/core/testing';
import {
  FormControl,
  FormGroup,
  ReactiveFormsModule,
  Validators,
} from '@angular/forms';
import { By } from '@angular/platform-browser';

import { ControlErrorsComponent } from './control-errors.component';

describe('ControlErrorComponent', () => {
  let fixture: ComponentFixture<object>;

  let input: HTMLInputElement;

  const setup = async (HostComponent: Type<any>) => {
    await TestBed.configureTestingModule({
      imports: [ReactiveFormsModule],
      declarations: [ControlErrorsComponent, HostComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(HostComponent);
    fixture.detectChanges();

    input = fixture.debugElement.query(By.css("[data-testid='input']")).nativeElement
  };

  describe('passing the control name', () => {
    @Component({
      template: `
        <form [formGroup]="form">
          <input formControlName="control" data-testid="input" />
          <app-control-errors controlName="control">
            <ng-template let-errors>
              <ng-container *ngIf="errors.required">required</ng-container>
            </ng-template>
          </app-control-errors>
        </form>
      `,
    })
    class HostComponent {
      public control = new FormControl<string>('', Validators.required);
      public form = new FormGroup({ control: this.control });
    }

    beforeEach(async () => {
      await setup(HostComponent);
    });

    describe('valid control', () => {
      it('renders nothing', () => {
        input.value = 'abc';
        input.dispatchEvent(new Event('input'));
        fixture.detectChanges();

        expect(fixture.nativeElement.textContent).toBe('');
      });
    });

    describe('invalid, pristine, untouched control', () => {
      it('renders nothing', () => {
        expect(fixture.nativeElement.textContent).toBe('');
      });
    });

    describe('invalid control, touched', () => {
      it('renders the template', () => {
        // Mark control as touched
        input.dispatchEvent(new FocusEvent('blur'));
        fixture.detectChanges();

        expect(fixture.nativeElement.textContent).toBe('required');
      });
    });

    describe('invalid control, dirty', () => {
      it('renders the template', () => {
        // Mark control as dirty
        input.dispatchEvent(new Event('input'));
        fixture.detectChanges();

        expect(fixture.nativeElement.textContent).toBe('required');
      });
    });
  });

  describe('without control name', () => {
    @Component({
      template: `
        <app-control-errors>
          <ng-template let-errors>
            <ng-container *ngIf="errors.required">required</ng-container>
          </ng-template>
        </app-control-errors>
      `,
    })
    class HostComponent { }

    it('throws an error', async () => {
      await TestBed.configureTestingModule({
        imports: [ReactiveFormsModule],
        declarations: [ControlErrorsComponent, HostComponent],
      }).compileComponents();

      fixture = TestBed.createComponent(HostComponent);

      expect(() => {
        fixture.detectChanges();
      }).toThrow();
    });
  });
});
